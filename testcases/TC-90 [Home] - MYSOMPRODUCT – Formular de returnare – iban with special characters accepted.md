## [Home] - MYSOMPRODUCT – Formular de returnare – iban with special characters accepted.
### **Test steps**:
1.Go to HOME - MYSOMPRODUCT - Formular de returnare.

2.Go to "IBAN-ul dumneavoastră "Vă rugăm să ne indicați un cont bancar valid în care să procesăm returnarea contravalorii produselor achiziționate".

3.Enter "???*(%$&*%".

### **Expected results**:
A message appears "Special characters are not accepted".

### ***Status***: $\textcolor{red}{\text{FAILED}}$
