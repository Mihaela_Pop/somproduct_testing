## [My Account] - LOGIN: Add valid email address and password
### **Test steps**:
1. Add email address.
2. Add password.
3. Log in.

### **Expected results**:
User is logged in.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
