## [Search] - Search product by invalid model.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search “lentila”.

### **Expected results**:
A message appears “Niciun produs pentru această căutare "lentila".

### ***Status***: $\textcolor{green}{\text{PASSED}}$
