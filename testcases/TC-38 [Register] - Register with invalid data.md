## [Register] - Register with invalid data.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert invalid data into "email", "prenume", " nume", " parola", "confirma parola" fields.
3. Check the Personal information agreement checkbox.
4. Click on "Trimite" button. 

### **Expected results**:
Error messages are displayed under every field.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
