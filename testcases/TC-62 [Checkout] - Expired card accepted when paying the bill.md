## [Checkout] - Expired card accepted when paying the bill.
### **Test steps**:
1. Select a product.
2. Add to cart.
3. Select delivery address.
4. Introduce an expired card date.

### **Expected results**:
When an expired card number is introduced an error appears.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
