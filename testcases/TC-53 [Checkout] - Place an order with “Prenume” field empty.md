## [Checkout] - Place an order with “Prenume” field empty.
### **Test steps**:
1. Leave “Prenume” field empty, other fields are filled.
2. Select credit card.
3. Click “Plaseaza comanda”.

### **Expected results**:
An error message appears “Toate campurile trebuie completate”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
