## [Shopping cart] - Add new item to the cart
 ### **Test steps**:
1. Select size.
2. Select colour.
3. Select quantity.
4. Add to cart.

### **Expected results**:
The new item is displayed in cart with the correct details: size, colour, image, quantity and price

### ***Status***: $\textcolor{green}{\text{PASSED}}$


