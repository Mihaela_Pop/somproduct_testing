## [Shopping cart] - Remove from cart: Remove an item from the cart
### **Test steps**:
Remove the item from cart.

### **Expected results**:
The item is no more available in cart. The cart total price is updated.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
