## [Shopping cart] - Quantity validation - quantity not available in stock
### **Test steps**:
Try to add out of stock item to cart.

### **Expected results**:
A message is displayed. No changes.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
