## [Wishlist] - Remove all items from Wishlist.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro
2. Add 3 products to wishlist.
3. Open wishlist and delete all 3 products.

### **Expected results**:
All items were removed, the wishlist was updated to empty.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
