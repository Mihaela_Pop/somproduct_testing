## [Wishlist] - Check if a non-registered user can see the correct content of the wishlist.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro
2. Add a product to wishlist.

### **Expected results**:
The product was added to wishlist.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
