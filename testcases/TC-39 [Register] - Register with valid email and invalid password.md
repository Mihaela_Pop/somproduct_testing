## [Register] - Register with valid email and invalid password.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert valid data into "email", "prenume", " nume".
3. Insert invalid password, e.g “a”. Check the Personal information agreement checkbox.
4. Click on "Trimite" button. 

### **Expected results**:
A message is displayed “Mai adăugați caractere sau ștergeți spațiile de la început sau sfârșit”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
