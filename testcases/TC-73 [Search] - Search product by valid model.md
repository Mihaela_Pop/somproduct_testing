## [Search] - Search product by valid model.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search “Scaun de gradina / terasa din aluminiu Magnesia Verde / Antracit, l60xA56xH86 cm”.

### **Expected results**:
“Scaun de gradina / terasa din aluminiu Magnesia Verde / Antracit, l60xA56xH86 cm” is displayed.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
