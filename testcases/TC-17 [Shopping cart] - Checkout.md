## [Shopping cart] - Checkout
### **Test steps**:
Click on checkout button.

### **Expected results**:
The user is redirected to the checkout page.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
