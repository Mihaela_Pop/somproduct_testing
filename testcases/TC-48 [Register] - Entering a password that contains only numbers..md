## [Register] - Entering a password that contains only numbers.
### **Test steps**:
1. Click on "Inregistrare".
2. Insert valid data into "email", "nume", " prenume".
3. Enter numbers in the password field, e.g “1234567”.
4. Check the Personal information agreement checkbox.
5. Click on "Inregistreaza-te" button.

### **Expected results**:
A message is displayed “ Parola trebuie sa contina cel putin 6 caractere, o majuscula, o minuscula, un numar sau un caracter special”.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
