## [Register] - Blank field for “confirma parola”.
### **Test steps**:
1. Click on "Inregistrare".
2. Insert valid data into "email", "nume", " prenume", " parola" fields.
3. Leave empty “Confirma parola”.
4. Check the Personal information agreement checkbox.
5. Click on "Inregistreaza-te" button.

### **Expected results**:
A message is displayed “Acest câmp este obligatoriu”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
