## [Search] - Search products by valid category name.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search “gradina”.

### **Expected results**:
Garden equipment is displayed.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
