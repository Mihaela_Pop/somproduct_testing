## [Register] - Register with an already registered email address and changed password.
### **Test steps**:
1. Click on “Inregistrare”.
2. Inside email field use an already registered email.
3. Fill the “prenume”,”nume”,fields. Add different password.
4. Check the Personal information agreement checkbox.  Click on “Trimite” button.

### **Expected results**:
A message is displayed “Adresa de email există deja”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
