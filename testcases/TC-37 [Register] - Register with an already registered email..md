## [Register] - Register with an already registered email.
### **Test steps**:
1. Click on “Inregistrare”.
2. Inside email field use an already registered email.
3. Fill the “prenume”,”nume”,”parola”,”confirma parola” fields.
4. Check the Personal information agreement checkbox.
5. Click on “Trimite” button.

### **Expected results**:
Error messages "Adresa de email exista in baza de date.Va rugam sa va logati" appear under the email field. 

### ***Status***: $\textcolor{green}{\text{PASSED}}$

