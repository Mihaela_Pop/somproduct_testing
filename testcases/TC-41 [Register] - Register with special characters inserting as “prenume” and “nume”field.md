## [Register] - Register with special characters inserting as “prenume” and “nume”field.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert data into "email", "prenume", " nume". When entering “prenume” insert “?” and “nume” insert “%”
3. Insert valid password. Check the Personal information agreement checkbox.
4. Click on "Trimite" button.

### **Expected results**:
A message is displayed " Insert letters, special characters are not accepted".

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
