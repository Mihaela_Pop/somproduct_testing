## [My Account] - Login with special characters added when writing email
### **Test steps**:
1. Add special characters when writing email
2. Write correct password

### **Expected results**: A message appears: Introdu o adresă de email validă. De exemplu ionpopescu@domeniu.com.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
