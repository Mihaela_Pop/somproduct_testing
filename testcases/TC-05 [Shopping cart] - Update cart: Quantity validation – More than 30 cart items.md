## [Shopping cart] - Update cart: Quantity validation – More than 30 cart items
### **Test steps**:
1. 30 items are already added in the cart
2. Add another item in the cart.

### **Expected results**:
A message is displayed, the user is not allowd to add more than 30 items. No changes.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
