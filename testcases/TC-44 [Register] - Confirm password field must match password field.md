## [Register] - Confirm password field must match password field.
### **Test steps**:
1. Click on "Inregistrare".
2. Insert valid data into "email", "nume", " prenume", " parola" fields.
3. Insert invalid data into “Confirma parola”
4. Check the Personal information agreement checkbox.
5. Click on "Inregistreaza-te" button.

### **Expected results**:
A message is displayed “Asigură-te că parolele coincid”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
