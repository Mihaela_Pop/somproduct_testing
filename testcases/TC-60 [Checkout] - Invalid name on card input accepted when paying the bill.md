## [Checkout] - Invalid name on card input accepted when paying the bill.
### **Test steps**:
1. Select a product.
2. Add to cart.
3. Select delivery address.
4. Introduce the name of the card holder.

### **Expected results**:
When writing special characters an error appears.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
