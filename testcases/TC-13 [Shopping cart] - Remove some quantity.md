## [Shopping cart] - Remove some quantity
### **Test steps**:
Existing items in the cart.

### **Expected results**:
The item total price and cart total price is updated.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
