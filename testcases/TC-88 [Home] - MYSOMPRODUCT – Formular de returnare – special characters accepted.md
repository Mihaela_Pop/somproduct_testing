## [Home] - MYSOMPRODUCT – Formular de returnare – special characters accepted.
### **Test steps**:
1. Go to HOME - MYSOMPRODUCT - Formular de returnare.

2. Enter "miha@yahoo.c".

3. Vă rugăm să introduceți numărul comenzii *Numărul comenzii este de forma #1000XXXX".

4. Enter "0000000000".

5. Ce produs doriți să returnați?

6. Enter: "???"

### **Expected results**:
Special characters are not accepted, user can write only letters.

### ***Status***: $\textcolor{red}{\text{FAILED}}$

