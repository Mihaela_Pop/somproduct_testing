## [My Account] - Modify account information (with Firefox)
### **Test steps**:
1. Add "Mihaela as "prenume".
2. Add "Marchis" as "nume".
3. Click " Salveaza".

### **Expected results**:
Nothing happens, the save buuton doesn’t work. The save button works with Chrome browser.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
