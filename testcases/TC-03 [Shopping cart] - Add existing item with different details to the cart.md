## [Shopping cart] - Add existing item with different details to the cart
### **Test steps**:
1. Select size.
2. Select quantity.
3. Add to cart.
4. Search the same item.
3. Change details: color, size.
4. Add to cart.

### **Expected results**:
The item is added to the cart again with correct details.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
