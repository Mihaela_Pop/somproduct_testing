## [Home] - Incorrect discounts on products - price doesn't match with the discount.
### **Test steps**:
1. Go to "https://www.somproduct.ro/pat-tapitat-cu-stofa-si-4-sertare-selma-gri.html" [^]
2. Calculate the discount

### **Expected results**:
A product "Pat tapitat cu stofa si 4 sertare Selma Gri " that has the price 5660 lei and has a 59% discount has the final price 2320.6 lei.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
