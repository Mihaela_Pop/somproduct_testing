## [Search] - Search using special characters.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search “?”

### **Expected results**:
A message appears “Niciun produs pentru această căutare".

### ***Status***: $\textcolor{RED}{\text{FAILED}}$

