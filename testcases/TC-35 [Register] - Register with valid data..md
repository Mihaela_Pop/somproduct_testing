## [Register] - Register with valid data.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert valid data into "email", "prenume", " nume", " parola", "confirma parola" fields.
3. Check the Personal information agreement checkbox.
4. Click on "Trimite" button.

### **Expected results**:
Account is created and a confirmation email is sent to the user.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
