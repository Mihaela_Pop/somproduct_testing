## [Home] - MYSOMPRODUCT – Formular de returnare – invalid quantity accepted.
### **Test steps**:
1.Go to HOME - MYSOMPRODUCT - Formular de returnare

2.Enter "miha@yahoo.c"

3.Vă rugăm să introduceți numărul comenzii *Numărul comenzii este de forma #1000XXXX".

4.Enter "0000000000".

5.Ce produs doriți să returnați?

6.Enter: "???".

7.Introduceți cantitatea "Introduceți numărul de bucăți pe care doriți să le returnați".

8.Enter: "10000000000000".

### **Expected results**:
Error message appears, invalid number.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
