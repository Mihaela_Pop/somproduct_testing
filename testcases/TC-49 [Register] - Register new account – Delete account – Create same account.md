## [Register] - Register new account – Delete account – Create same account.
### **Test steps**:
1. Click on "Inregistrare".
2. Enter valid data to register new account. Click on “Inregistreaza-te” button.
3. Delete new account.
4. Register with the old account.
5. Click on "Inregistreaza-te" button.

### **Expected results**:
A message is displayed “Vă mulțumim pentru înregistrare!".

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
