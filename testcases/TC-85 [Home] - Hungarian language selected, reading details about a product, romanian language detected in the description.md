## [Home] - Hungarian language selected, reading details about a product, romanian language detected in the description.
### **Test steps**:
1. Select Hungarian language.
2. Select a product.
3. Read details about the product.

### **Expected results**:
When the selected language is Hungarian, all the text is in Hungarian.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
