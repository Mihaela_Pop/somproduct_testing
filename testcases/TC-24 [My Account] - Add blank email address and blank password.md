## [My Account] - Add blank email address and blank password.
### **Test steps**:
1. Add blank email address.
2. Add blank password.
3. Log in.

### **Expected results**:
User is not logged in. A message appears: invalid authentication data.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
