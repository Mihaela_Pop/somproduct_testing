## [Register] - Register with negative numbers inserting as “nume” and “prenume” field.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert data into "email", "prenume", " nume". When entering “prenume” insert “-1” and “nume” insert “-3”
3. Insert valid password. Check the Personal information agreement checkbox.
4. Click on "Trimite" button.

### **Expected results**:
A message is displayed “Negative numbers are not accepted when writing “prenume” and “nume”, only letters are accepted.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
