## [Shopping cart] - Quantity validation - not a number
### **Test steps**:
1. Select an item in the cart.
2. Modify the quantity with not a number. 

### **Expected results**: 
When introducing letters where we introduce numbers, number 1 appears.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
