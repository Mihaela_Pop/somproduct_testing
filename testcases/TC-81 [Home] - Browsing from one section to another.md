## [Home] - Browsing from one section to another.
### **Test steps**:
1. Go to www.somproduct.ro.
2. From “AJUTOR” select “Cum cumpar”.

### **Expected results**:
The page regarding "Cum cumpar" is displayed.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
