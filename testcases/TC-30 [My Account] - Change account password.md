## [My Account] - Change account password
### **Test steps**:
1. Change account information.
2. Click on "Modifica Parola".
3. Insert a new valid password into " Parola noua" field.
4. Confirm the new password.
5. Insert the current password.
6. Click “Salveaza”.

### **Expected results**:
The new password is accepted and redirects me to "contul meu" page.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$

