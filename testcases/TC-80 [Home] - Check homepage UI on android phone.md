## [Home] - Check homepage UI on android phone.
### **Test steps**:
1. Go to www.somproduct.ro.
2. User opens the phone's wireframes of the homepage. Check if the UI and the wireframes match.

### **Expected results**:
The phone's UI matches the phone's wireframes without any errors or differences.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
