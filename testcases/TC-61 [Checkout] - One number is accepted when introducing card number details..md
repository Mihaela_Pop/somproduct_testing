## [Checkout] - One number is accepted when introducing card number details.
### **Test steps**:
1. Select a product.
2. Add to cart.
3. Select delivery address.
4. Introduce just a number, e.g. "1" in the card number field.

### **Expected results**:
When writing just a number in the field an error appears.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
