## [Checkout] - Place an order with all fields filled.
### **Test steps**:
1. Fill in all fields.
2. Select credit card.
3. Click “Plaseaza comanda”.

### **Expected results**:
The user is redirected to secure.mobilpay.ro.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
