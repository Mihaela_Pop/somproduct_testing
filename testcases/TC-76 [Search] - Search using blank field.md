## [Search] - Search using blank field.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search blank field.

### **Expected results**:
Nothing happens.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
