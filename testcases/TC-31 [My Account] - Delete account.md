## [My Account] - Delete account
### **Test steps**:
1. Click “My account” – “Data protection”.
2. Delete account.

### **Expected results**:
The account is deleted.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
