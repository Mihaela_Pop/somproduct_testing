## [Wishlist] - Check if a registered user can see the correct content of the wishlist.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro
2. Add a product to cart.
3. Open wishlist.

### **Expected results**:
Wishlist is displayed with all items in it.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
