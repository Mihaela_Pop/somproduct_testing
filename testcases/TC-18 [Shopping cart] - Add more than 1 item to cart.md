## [Shopping cart] - Add more than 1 item to cart
### **Test steps**:
1. Select an item.
2. Add 2 items to cart.

### **Expected results**:
More items can be added to cart.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
