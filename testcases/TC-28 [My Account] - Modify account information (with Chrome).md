## [My Account] - Modify account information (with Chrome)
### **Test steps**:
1. Add "?" as "prenume".
2. Add "12" as "nume".
3. Click " Salveaza".

### **Expected results**: 
Invalid data, only letters accepted.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
