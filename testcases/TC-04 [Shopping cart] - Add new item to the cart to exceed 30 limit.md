## [Shopping cart] - Add new item to the cart to exceed 30 limit
### **Test steps**:
1. Have 30 items in the cart. Total price is shown
2. Add one more item in the cart. 

### **Expected results**:
A warning message is diplayed. The new item is not added to the cart. The cart total price is updated.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
