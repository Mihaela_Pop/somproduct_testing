## [Register] - Register new account without receiving a link to activate it.
### **Test steps**:
1. Register with email address.
2. Click on “Inregistreaza-te” button.

### **Expected results**:
A link is sent to the user to confirm the registration.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
