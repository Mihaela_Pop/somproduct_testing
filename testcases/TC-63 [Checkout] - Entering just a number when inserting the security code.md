## [Checkout] - Entering just a number when inserting the security code.
### **Test steps**:
1. Select a product.
2. Add to cart.
3. Select delivery address.
4. Introduce a number in the security code field.

### **Expected results**:
When writing just a number in the security code field an error appears that says "enter 3 or 4 numbers in the field".

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
