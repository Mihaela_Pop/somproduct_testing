## [Home] - Redirect to another website.
### **Test steps**:
1. Scroll down to the bottom of the page.
2. From “AJUTOR” select “ANPC” option.

### **Expected results**:
ANPC website is opened into a new tab.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
