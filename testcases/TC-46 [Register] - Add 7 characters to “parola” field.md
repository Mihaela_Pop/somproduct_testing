## [Register] - Add 7 characters to “parola” field.
### **Test steps**:
1. Click on "Inregistrare".
2. Insert valid data into "email", "nume", " prenume".
3. Add 7 letters in the “parola” field.
4. Check the Personal information agreement checkbox.
5. Click on "Inregistreaza-te" button.

### **Expected results**:
The password is accepted.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
