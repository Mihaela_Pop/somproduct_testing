## [Register] - Register with invalid email and valid password.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert invalid data into "email".
3. Insert valid password, name, surname.Check the Personal information agreement checkbox.
4. Click on "Trimite" button. 

### **Expected results**:
A message is displayed “Introdu o adresă de email validă. De exemplu mihai.andone@domeniu.com.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
