## [Shopping cart] - Update existing item quantity (integer 1-29, not exceeding 30 cart limit)
### **Test steps**:
Modify the quantity but not exceed 100 limit.

### **Expected results**:
The item total price and cart price are corectly updated.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
