## [Search] - Search products by invalid category name.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search “adidasi”.

### **Expected results**:
A message appears “Niciun produs pentru această căutare "adidasi".

### ***Status***: $\textcolor{green}{\text{PASSED}}$
