## [Shopping cart] - Update size
### **Test steps**:
Update the size of the item in the cart.

### **Expected results**:
This action is not allowed.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
