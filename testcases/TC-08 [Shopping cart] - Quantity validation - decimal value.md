## [Shopping cart] - Quantity validation - decimal value
### **Test steps**:
1. Select an item in the cart.
2. Modify the quantity with decimal value. 

### **Expected results**:
A message is displayed. No changes.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
