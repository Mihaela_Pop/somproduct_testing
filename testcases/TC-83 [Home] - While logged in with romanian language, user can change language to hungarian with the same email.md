## [Home] - While logged in with romanian language, user can change language to hungarian with the same email.
### **Test steps**:
1. Logged in with Romanian language.
2. Select Hungarian language.

### **Expected results**:
When changing language user has a different email address.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
