## [My Account] - Login with deleted account
### **Test steps**:
1. Click on "Autentificare button".
2. Fill "Adresa de email" and "Parola" fields.
3. Click “Autentificare”.

### **Expected results**: 
Error message appears that says “Date de autentificare invalide.”

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
