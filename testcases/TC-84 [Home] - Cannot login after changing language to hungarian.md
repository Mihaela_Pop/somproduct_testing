## [Home] - Cannot login after changing language to hungarian.
### **Test steps**:
1. Select a product.
2. Add to cart.
3. Select delivery address.
4. Introduce a number in the security code field.

### **Expected results**:
Logged in successfully.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
