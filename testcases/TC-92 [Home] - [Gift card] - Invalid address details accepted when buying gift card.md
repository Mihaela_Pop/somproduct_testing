## [Home] - [Gift card] - Invalid address details accepted when buying gift card.
### **Test steps**:
1. Add gift card to cart.
2. Finish shopping.
3. Insert delivery address.

### **Expected results**:
When a special character or just a letter is shown in the delivery section an error appears and doesn't let you continue with the process.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
