## [Shopping cart] - Set 0 as quantity
### **Test steps**:
1. Select quantity 0 for the existing item.
2. Update cart. 

### **Expected results**:
The item is removed from cart.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
