## [Home] - [Subcategory] When selecting a specific subcategory, products don't exist there.
### **Test steps**:
1. Select category "Electrocasnice".
2. Select subcategory "Hote".

### **Expected results**:
Products exist in this subcategory.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
