## [Wishlist] - The number of favorite items missing.
### **Test steps**:
1. Go to a particular item.
2. Press add to cart button. 

### **Expected results**:
At least one item is shown in to the wish list heart icon.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
