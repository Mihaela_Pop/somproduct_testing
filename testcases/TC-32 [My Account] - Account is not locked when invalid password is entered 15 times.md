## [My Account] - Account is not locked when invalid password is entered 15 times
### **Test steps**:
1. Write a valid username.
2. Write 15 invalid passwords.

### **Expected results**:
After entering 5 invalid passwords a warning message appears and the account is locked.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
