## [Search] - Search using special negative numbers, e.g. “-2”.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Search “-2”.

### **Expected results**:
A message appears “Niciun produs pentru această căutare".

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
