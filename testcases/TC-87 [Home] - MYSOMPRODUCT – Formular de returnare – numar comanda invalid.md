## [Home] - MYSOMPRODUCT – Formular de returnare – numar comanda invalid.
### **Test steps**:
1.Go to HOME - MYSOMPRODUCT - Formular de returnare.

2.Enter "miha@yahoo.c".

3.Vă rugăm să introduceți numărul comenzii *Numărul comenzii este de forma #1000XXXX".

4.Enter "0000000000".

### **Expected results**:
Error message appears, “bill number not valid”.

### ***Status***: $\textcolor{red}{\text{FAILED}}$
