## [Wishlist] - Add more than one item from the Wishlist to the cart.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro
2. Add 2 products to wishlist.
3. Open wishlist and add items to cart.

### **Expected results**:
The items are added to the cart, the products are remaining also in the wishlist.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
