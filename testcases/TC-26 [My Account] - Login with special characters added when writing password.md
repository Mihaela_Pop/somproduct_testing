## [My Account] - Login with special characters added when writing password
### **Test steps**:
1. Add valid email.
2. Add special characters when writing pasword.

### **Expected results**: 
A message appears “Date de autentificare invalide”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
