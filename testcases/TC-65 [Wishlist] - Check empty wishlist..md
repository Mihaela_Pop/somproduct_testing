## [Wishlist] - Check empty wishlist.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro.
2. Open wishlist button. 

### **Expected results**:
The wishlist is empty.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
