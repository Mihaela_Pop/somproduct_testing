## [Wishlist] - Remove an item from the Wishlist.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro
2. Add a product to wishlist.
3. Open wishlist and delete item from it.

### **Expected results**:
The item is removed and the wishlist is updated.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
