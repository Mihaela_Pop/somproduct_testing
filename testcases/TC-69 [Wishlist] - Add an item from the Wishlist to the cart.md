## [Wishlist] - Add an item from the Wishlist to the cart.
### **Test steps**:
1. Open the browser and access the site www.somproduct.ro
2. Add 1 products to wishlist.
3. Open wishlist and add item to cart.

### **Expected results**:
The item is added to cart, but the item also remains in wishlist.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
