## [Register] - Register with blank “nume” and “prenume”.
### **Test steps**:
1. Click on “Inregistrare”.
2. Insert blank “nume” and blank “prenume”.
3. Insert valid password and email address.Check the Personal information agreement checkbox.
4. Click on "Trimite" button. 

### **Expected results**:
A message is displayed “Acest câmp este obligatoriu.”

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
