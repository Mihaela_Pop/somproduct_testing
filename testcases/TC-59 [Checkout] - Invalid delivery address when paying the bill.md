## [Checkout] - Invalid delivery address when paying the bill.
### **Test steps**:
1. Add item to cart.
2. Finish shopping.
3. Insert delivery address.

### **Expected results**:
When just a letter is shown in the delivery section an error appears and doesn't let you continue with the process.

### ***Status***: $\textcolor{RED}{\text{FAILED}}$
