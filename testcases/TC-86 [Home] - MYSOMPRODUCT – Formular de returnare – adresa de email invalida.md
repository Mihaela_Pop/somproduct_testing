## [Home] - MYSOMPRODUCT – Formular de returnare – adresa de email invalida.
### **Test steps**:
1. Go to HOME - MYSOMPRODUCT - Formular de returnare.
2. Enter "miha@yahoo.c".

### **Expected results**:
A message appears "Email address not valid".

### ***Status***: $\textcolor{red}{\text{FAILED}}$
