## [My Account] - Login with Facebook
### **Test steps**:
1. Click “Login”.
2. Choose “Conectare cu Facebook”.

### **Expected results**:
A message appears “our Facebook account is now connected to your store account. You can now login using our Facebook Login button or using store account credentials you will receive to your email address.”

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
