## [Home] - Check UI and content on the homepage.
### **Test steps**:
1. Go to www.somproduct.ro.
2. Open the wireframes of the HomePage.
3. Check if the UI and the wireframes match.

### **Expected results**:
UI matches the wireframes without any errors or differences.


### ***Status***: $\textcolor{green}{\text{PASSED}}$
