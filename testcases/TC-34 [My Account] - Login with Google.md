## [My Account] - Login with Google
### **Test steps**:
1. Click “Login”.
2. Choose “Conectare cu Google”.

### **Expected results**:
A message appears “Se pare că ai deja un cont de client. Contul tău Google este acum conectat cu contul de client din magazin.”

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
