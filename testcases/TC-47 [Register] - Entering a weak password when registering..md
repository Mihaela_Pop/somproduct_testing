## [Register] - Entering a weak password when registering.
### **Test steps**:
1. Click on "Inregistrare".
2. Insert valid data into "email", "nume", " prenume".
3. Enter a weak password, e.g “12”.
4. Check the Personal information agreement checkbox.
5. Click on "Inregistreaza-te" button.

### **Expected results**:
A message is displayed “Adaugati minim 6 caractere”.

### ***Status***: $\textcolor{GREEN}{\text{PASSED}}$
