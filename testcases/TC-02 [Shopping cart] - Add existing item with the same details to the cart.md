## [Shopping cart] - Add existing item with the same details to the cart
### **Test steps**:
1. Select size.
2. Select colour.
3. Select quantity.
4. Add to cart.
3. Select the same size and color.
4. Add to cart.

### **Expected results**:
The product and cart total price is updated.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
