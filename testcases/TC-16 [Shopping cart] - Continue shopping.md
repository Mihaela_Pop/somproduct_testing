## [Shopping cart] - Continue shopping
### **Test steps**:
1. Click on continue shopping.
2. Click on shopping cart button. 

### **Expected results**:
 The user is redirected to the search page. The previously selected item is still in the cart.

### ***Status***: $\textcolor{green}{\text{PASSED}}$
