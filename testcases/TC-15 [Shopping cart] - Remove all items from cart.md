## [Shopping cart] - Remove all items from cart
### **Test steps**:
1. Remove first item
2. Remove second item. 

### **Expected results**:
1. The item is removed frim cart. The total price is updated.
2. The item is removed from cart. The total price is 0. 

### ***Status***: $\textcolor{green}{\text{PASSED}}$
