# Somproduct_testing_project

<p>I tested a website called "www.somproduct.ro" and documented my findings in test cases and bug reports.<p>
🧑‍💻 Software Testing has caught my attention and I'm eager to apply my expertise to enhance the quality of products.
<p>📚 I'm presently broadening my knowledge in this domain, absorbing new information about testing and automation.
<p>🤝 I'm seeking opportunities to collaborate on QA projects and work as a part of a team.
<p>📧 You can contact me through my email address to get in touch: marchis_mihaela@yahoo.com
