 **Tools used:**  Mantis Bug Tracker and TestLink.

- 32 bugs were found, 30 remain open, 2 are closed. 

- <p> 93 Test cases were written, 67 passed and 26 failed.</p>
| Passed | Failed |
| ------ | ------ |
| TC-01| TC-28 |
| TC-02| TC-29| 
| TC-03| TC-32 |
| TC-04| TC-41|
| TC-05| TC-42 
| TC-06| TC-48|
| TC-07| TC-59| 
| TC-08  |TC-60|
| TC-09| TC-61|
| TC-10| TC-62|
| TC-11| TC-63| 
| TC-12| TC-64|
| TC-13| TC-71|
| TC-14| TC 77| 
| TC-15| TC-78| 
| TC-16| TC-83|
| TC-17| TC-84|
| TC-18| TC-85| 
| TC-19| TC-86|
| TC-20| TC-87|
| TC-21| TC-88| 
| TC-22| TC-89| 
| TC-23| TC-90|
| TC-24| TC-91|
| TC-25| TC-92|
| TC-26| TC-93|            
| TC-27|      | 
| TC-30|      |
| TC-31|      |        
| TC-33|      | 
| TC-34|      | 
| TC-35|      |
| TC-36|      |        
| TC-37|      | 
| TC-38|      | 
| TC-39|      |
| TC-40|      |        
| TC-43|      | 
| TC-44|      | 
| TC-45|      |
| TC-46|      |        
| TC-47|      | 
| TC-49|      |
| TC-50|      |        
| TC-51|      | 
| TC-52|      | 
| TC-53|      |
| TC-54|      |        
| TC-55|      |        
| TC-56|      |
| TC-57|      |        
| TC-58|      | 
| TC-65|      |
| TC-66|      |        
| TC-67|      |    
| TC-68|      | 
| TC-69|      |
| TC-70|      |   
| TC-72|      |   
| TC-73|      | 
| TC-74|      |
| TC-75|      |        
| TC-76|      |        
| TC-79|      |
| TC-80|      |        
| TC-81|      | 
| TC-82|      |


**Test cases overview:**

![diagram](https://gitlab.com/Mihaela_Pop/somproduct_testing/-/raw/main/project_pictures/x_mind.png?inline=false)

![diagram](https://gitlab.com/Mihaela_Pop/somproduct_testing/-/raw/main/project_pictures/pie_chart.png?inline=false)


- Equivalence partitioning and boundary value analysis helped finding new bugs.

**Testing approach:** Ad-hoc Testing to decide if the application is worth further testing,
 Localization Testing, Compatibility Testing, Component Testing, Integration, System, System Integration Testing, Regression Testing, UI Testing, Security Testing, Exploratory testing.



